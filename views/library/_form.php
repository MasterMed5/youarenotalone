<?php

/*
 * 22.11.2020
 * File: _form.php
 * Encoding: UTF-8
 * Project: Test task for "You are not alone"
 * 
 * Author: Gafuroff Alexandr 
 * E-mail: gafuroff.al@yandex.ru
 */

use yii\helpers\{Html, ArrayHelper};
use yii\widgets\{Pjax, ActiveForm};

/* @var $this yii\web\View */
/* @var $model app\models\Books */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-form">

    <?php Pjax::begin(); ?>
    
        <?php if($formStatus == 'success'): ?>
            <div class="alert alert-success" role="alert">
                Данные о книге успешно сохранены.
            </div>
        <?php endif; ?>
    
        <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'enctype' => 'multipart/form-data']]); ?>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'date')->textInput(['type' => 'date']) ?>

        <?= $form->field($model, 'author')->dropDownList(
                    ArrayHelper::map($authors, 'id', 'name'),
                    [
                        'prompt' => 'Укажите автора'
                    ]
            ) ?>

        <?= $form->field($model, 'genre')->dropDownList(
                    ArrayHelper::map($genries, 'id', 'name'),
                    [
                        'prompt' => 'Укажите жанр'
                    ]
            ) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    
        <?php if($model->image == ''): ?>
            <?= $form->field($file, 'imageFile')->fileInput()->label('Загрузить обложку'); ?>
        <?php else: ?>
            <?= Html::img('/web/upload/images/book-thumbs/' . $model->image, [
                        'class' => 'img-responsive img-thumbnail'
                    ]); ?>
            <?= $form->field($file, 'imageFile')->fileInput()->label('Заменить обложку'); ?>
        <?php endif; ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>

</div>
