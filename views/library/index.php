<?php

/*
 * 22.11.2020
 * File: index.php
 * Encoding: UTF-8
 * Project: Test task for "You are not alone"
 * 
 * Author: Gafuroff Alexandr 
 * E-mail: gafuroff.al@yandex.ru
 */

use yii\helpers\{Html, ArrayHelper};
use yii\widgets\Pjax;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Библиотека';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить книгу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(['id' => 'books', 'timeout' => false, 'enablePushState' => true, 'clientOptions' => ['method' => 'POST']]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],
                'id',
                'title',
                [
                    'attribute' => 'description',
                    'contentOptions' => ['style' => 'width: 40%'],
                    'format' => 'ntext'
                ],
                'date',                
                [
                    'attribute' => 'authorName',
                    'filter' => ArrayHelper::map($authors, 'id', 'name'),
                ],
                [
                    'attribute' => 'genreName',
                    'filter' => ArrayHelper::map($genries, 'id', 'name'),
                ],
                //'image',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>
