<?php

/*
 * 22.11.2020
 * File: view.php
 * Encoding: UTF-8
 * Project: Test task for "You are not alone"
 * 
 * Author: Gafuroff Alexandr 
 * E-mail: gafuroff.al@yandex.ru
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Books */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Библиотека', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="books-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить эту книгу?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'date',
            'authorName',
            'genreName',
            'description:ntext',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::img('/upload/images/book-thumbs/' . $data->image, [
                        'class' => 'img-responsive img-thumbnail'
                    ]);

                }
            ], 
        ],
    ]) ?>

</div>
