<?php

/*
 * 22.11.2020
 * File: LibraryController.php
 * Encoding: UTF-8
 * Project: Test task for "You are not alone"
 * 
 * Author: Gafuroff Alexandr 
 * E-mail: gafuroff.al@yandex.ru
 */

namespace app\controllers;

use Yii;
use app\models\{Books, BooksSearch, Authors, Genries, Upload};
use yii\web\{Controller, NotFoundHttpException, UploadedFile};
use yii\filters\VerbFilter;

/**
 * LibraryController implements the CRUD actions for Books model.
 */
class LibraryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Books models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new BooksSearch();

        if ($searchModel->load(Yii::$app->request->post())) {
            $searchModel = new BooksSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->post());
        } else {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        }
        
        $authors = Authors::find()->all();
        $genries = Genries::find()->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'authors' => $authors,
            'genries' => $genries
        ]);
    }

    /**
     * Displays a single Books model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Books model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Books();
        $file = new Upload();
        
        $formStatus = false;

        if(Yii::$app->request->isAjax AND Yii::$app->request->post('Books')) {
            $model->attributes = Yii::$app->request->post('Books');
            $file->imageFile = UploadedFile::getInstance($file, 'imageFile');
            $model->image = $file->upload();
            if($model->image AND $model->validate() AND $model->save()) {
                $formStatus = 'success';
                $model = new Books();
            } 
        }
        
        $authors = Authors::find()->all();
        $genries = Genries::find()->all();

        return $this->render('create', compact('model', 'authors', 'genries', 'file', 'formStatus'));
    }

    /**
     * Updates an existing Books model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $file = new Upload();
        
        $formStatus = false;
        $oldImage = null;

        if(Yii::$app->request->isAjax AND Yii::$app->request->post('Books')) {
            $model->attributes = Yii::$app->request->post('Books');
            if($model->image == '') {
                $file->skipOnEmpty = true;
            }
            $file->imageFile = UploadedFile::getInstance($file, 'imageFile');
            if(!is_null($file->imageFile)) {
                $oldImage = $model->image;
                $model->image = $file->upload();
            }            
            if($model->image AND $model->validate() AND $model->save()) {
                $formStatus = 'success';
                if(!is_null($oldImage)) {
                    $file->deleteOld($oldImage);
                }
                $model = new Books();
            } 
        }
        
        $authors = Authors::find()->all();
        $genries = Genries::find()->all();

        return $this->render('update', compact('model', 'authors', 'genries', 'file', 'formStatus'));
    }

    /**
     * Deletes an existing Books model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $file = new Upload();
        $file->deleteOld($model->image);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Books model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Books the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Books::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
