<?php

/*
 * 22.11.2020
 * File: Upload.php
 * Encoding: UTF-8
 * Project: Test task for "You are not alone"
 * 
 * Author: Gafuroff Alexandr 
 * E-mail: gafuroff.al@yandex.ru
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * Description of Upload
 *
 * @author Александр
 */
class Upload extends Model {
    
    /**
     * @var UploadedFile[]
     */
    public $imageFile;
    public $skipOnEmpty = false;
    
    protected $newWidth = '128';

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => $this->skipOnEmpty, 'extensions' => 'png, jpg'],
        ];
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $path = Yii::getAlias('@webroot') . '/upload/images/book-thumbs/';
            $filename = str_replace(['0.', ' '], ['', '_'], microtime()) . '.' . $this->imageFile->extension;
            $filePath = $path . $filename;
            $this->imageFile->saveAs($filePath);
            $imagine = Image::getImagine();
            $image = $imagine->open($filePath);
            $image->resize(new Box($this->newWidth, $this->getFinalyHeight($filePath, $this->newWidth)))->save($filePath, ['quality' => 100]);
            return $filename;
        } else {
            return false;
        }
    }   
    
    public function deleteOld($imageName) {
        $path = Yii::getAlias('@webroot') . '/upload/images/book-thumbs/';
        $filePath = $path . $imageName;
        if(file_exists($filePath)) {
            unlink($filePath);
        }        
    }
    
    protected function getFinalyHeight($image, $to_width)
    {
        list($owidth, $oheight) = getimagesize($image);
        $q = $owidth/$oheight;
        return round($to_width/$q);
    }
    
}
