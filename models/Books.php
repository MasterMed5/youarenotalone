<?php

/*
 * 22.11.2020
 * File: Books.php
 * Encoding: UTF-8
 * Project: Test task for "You are not alone"
 * 
 * Author: Gafuroff Alexandr 
 * E-mail: gafuroff.al@yandex.ru
 */

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Description of Books
 *
 * @author Александр
 */
class Books extends ActiveRecord 
{
    public static function tableName() 
    {
        return 'books';
    }
    
    public function attributeLabels() 
    {
        return [
            'id' => '№',
            'title' => 'Название',   
            'date' => 'Дата',
            'authorName' => 'Автор',
            'genreName' => 'Жанр',
            'author' => 'Автор',
            'genre' => 'Жанр',
            'description' => 'Описание',
            'image' => 'Обложка',
        ];
    }
    
    public function rules()
    {
        return [
            [['title', 'date', 'author', 'genre', 'description', 'image'], 'required'],
            ['title', 'string', 'length' => [20, 150]],
            ['description', 'string', 'length' => [100, 33000]],
            ['author', 'compare', 'compareValue' => 0, 'operator' => '!=', 'type' => 'number', 'message' => 'Укажите автора'],
            ['genre', 'compare', 'compareValue' => 0, 'operator' => '!=', 'type' => 'number', 'message' => 'Укажите жанр'],
            ['date', 'compare', 'compareValue' => '', 'operator' => '!=', 'type' => 'string', 'message' => 'Укажите дату издания']
        ];
    }
    
    /**
     * Declare a link with the authors table
     */
    public function getAuthors() 
    {        
        return $this->hasOne(Authors::className(), ['id' => 'author']);
    }
    
    /**
     * Declare a link with the genries table
     */
    public function getGenries() 
    {        
        return $this->hasOne(Genries::className(), ['id' => 'genre']);
    }
    
    /**
     * get attribute value genreName from linked table
     */
    public function getAuthorName() 
    {
        return $this->authors->name;
    }
    
    /**
     * get attribute value authorName from linked table
     */
    public function getGenreName() 
    {
        return $this->genries->name;
    }   
    
}
