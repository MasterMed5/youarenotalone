<?php

/*
 * 22.11.2020
 * File: Genries.php
 * Encoding: UTF-8
 * Project: Test task for "You are not alone"
 * 
 * Author: Gafuroff Alexandr 
 * E-mail: gafuroff.al@yandex.ru
 */

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Description of Genries
 *
 * @author Александр
 */
class Genries extends ActiveRecord 
{
    public static function tableName() 
    {
        return 'genries';
    }
}
