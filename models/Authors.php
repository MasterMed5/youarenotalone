<?php

/*
 * 22.11.2020
 * File: Authors.php
 * Encoding: UTF-8
 * Project: Test task for "You are not alone"
 * 
 * Author: Gafuroff Alexandr 
 * E-mail: gafuroff.al@yandex.ru
 */

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Description of Authors
 *
 * @author Александр
 */
class Authors extends ActiveRecord 
{
    public static function tableName() 
    {
        return 'authors';
    }
}
