# Для того чтобы запустить тестовое задние на локальной машине.

РАЗВЕРТЫВАНИЕ НА ЛОКАЛЬНОЙ МАШИНЕ 
---------------------------------

Для установки проекта на сервер, необходимо проследовать по следующим пунктам 
инструкции.


### 1) Получение проекта на локальную или иную машину.

Для получения проекта на локальную машину  
клонирурте проект из git репозитория, для это необходимо перейти в целевую папку, в
которой планируется разместить приек и через git клиент. Введите команду **«git clone»** 
и URL репозитория в командную строку, а зависимости от используемого протокола
соединения:

~~~
git clone https://<ВАШ ЛОГИН>@bitbucket.org/MasterMed5/youarenotalone.git
~~~

или

~~~
git clone git@bitbucket.org:MasterMed5/youarenotalone.git
~~~

### 2) Загрузка ядра фреймворка, подключаемых компонентов и зависимостей.

Для получения ядра фрейворка, всех подключаемых компонентов и зависимостей, 
находясь в той же папке с проектом (если у вас установлен композер), в консоли 
нужно ввести команду:

~~~
composer install
~~~

### 3) Настройка подключения к базе данных.

Файлы находятся в директори **config**, настройки подключения к базе данных находятся в файле **db.php**:
~~~
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=<Название базы>',
    'username' => '<Имя пользователя>',
    'password' => '<Пароль>',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    'enableSchemaCache' => true, 
    'schemaCacheDuration' => 3600,
    'schemaCache' => 'cache',
];
~~~

### 4) Создание структуры базы данных и запись тестовых данных в неё.

После исполнения всех выше перечисленных пунктов, последним этапом нужно актуализировать 
базу данных вместе со структурой таблиц и хранящимися в них данными. Для этого, 
в терминале, находять в папке проекта, нужно запустить миграцию следующей командой:

~~~
yii migrate
~~~

После чего нажать Enter;
Консольное приложение выведет список актуальных миграций и запрос на исполнение.
На запрос системы написать **yes, y, YES, Y и т.п.** и дождаться окончания работы по применению всех 
актуальных миграций, и записи тестовых данных;

**Обратите внимание** так как запись тестовых данных в последней миграции идет через 
API сервиса Lorem Ipsum, формирование 10000 требуемых срок может занять значительное количество 
времени, поэтому если не хотите ждать, можете воспользоваться дампом в папке /web/test.sql

### 5) Настройка сервера.

Для того чтобы фреймворк работал стаблильно, и не возникало ошибок при живом тестировании
этого задания, настройте ваш тестовый сервер на прямое обращение к индексным файлам 
в папке web и раскоментируйте пункты файла .htaccess находящиеся в ней. Если нет желания 
возиться с настройками сервера, можете сделать это перенаправление с моммощью двух файлов 
.htaccess согласно [этой инструкции](https://webhamster.ru/mytetrashare/index/mtb0/1490104869nyjp1wvoqk)

### 5) Если вы запускаете этот тестовый проект на Linux машине.

Настройе права доступа с помощью утилиты chmod к следующим файлам и директориям:
~~~
0777 runtime
0777 web/assets
0755 yii 
~~~


 

# Инструкция по установке и работе с чистым проектом, поставляемая создателями фреймворка.

<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 Basic Project Template</h1>
    <br>
</p>

Yii 2 Basic Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
rapidly creating small projects.

The template contains the basic features including user login/logout and a contact page.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application.

[![Latest Stable Version](https://img.shields.io/packagist/v/yiisoft/yii2-app-basic.svg)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Total Downloads](https://img.shields.io/packagist/dt/yiisoft/yii2-app-basic.svg)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![build](https://github.com/yiisoft/yii2-app-basic/workflows/build/badge.svg)](https://github.com/yiisoft/yii2-app-basic/actions?query=workflow%3Abuild)

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.6.0.


INSTALLATION
------------

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
composer create-project --prefer-dist yiisoft/yii2-app-basic basic
~~~

Now you should be able to access the application through the following URL, assuming `basic` is the directory
directly under the Web root.

~~~
http://localhost/basic/web/
~~~

### Install from an Archive File

Extract the archive file downloaded from [yiiframework.com](http://www.yiiframework.com/download/) to
a directory named `basic` that is directly under the Web root.

Set cookie validation key in `config/web.php` file to some random secret string:

```php
'request' => [
    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
    'cookieValidationKey' => '<secret random string goes here>',
],
```

You can then access the application through the following URL:

~~~
http://localhost/basic/web/
~~~


### Install with Docker

Update your vendor packages

    docker-compose run --rm php composer update --prefer-dist
    
Run the installation triggers (creating cookie validation code)

    docker-compose run --rm php composer install    
    
Start the container

    docker-compose up -d
    
You can then access the application through the following URL:

    http://127.0.0.1:8000

**NOTES:** 
- Minimum required Docker engine version `17.04` for development (see [Performance tuning for volume mounts](https://docs.docker.com/docker-for-mac/osxfs-caching/))
- The default configuration uses a host-volume in your home directory `.docker-composer` for composer caches


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `config/` directory to customize your application as required.
- Refer to the README in the `tests` directory for information specific to basic application tests.


TESTING
-------

Tests are located in `tests` directory. They are developed with [Codeception PHP Testing Framework](http://codeception.com/).
By default there are 3 test suites:

- `unit`
- `functional`
- `acceptance`

Tests can be executed by running

```
vendor/bin/codecept run
```

The command above will execute unit and functional tests. Unit tests are testing the system components, while functional
tests are for testing user interaction. Acceptance tests are disabled by default as they require additional setup since
they perform testing in real browser. 


### Running  acceptance tests

To execute acceptance tests do the following:  

1. Rename `tests/acceptance.suite.yml.example` to `tests/acceptance.suite.yml` to enable suite configuration

2. Replace `codeception/base` package in `composer.json` with `codeception/codeception` to install full featured
   version of Codeception

3. Update dependencies with Composer 

    ```
    composer update  
    ```

4. Download [Selenium Server](http://www.seleniumhq.org/download/) and launch it:

    ```
    java -jar ~/selenium-server-standalone-x.xx.x.jar
    ```

    In case of using Selenium Server 3.0 with Firefox browser since v48 or Google Chrome since v53 you must download [GeckoDriver](https://github.com/mozilla/geckodriver/releases) or [ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/downloads) and launch Selenium with it:

    ```
    # for Firefox
    java -jar -Dwebdriver.gecko.driver=~/geckodriver ~/selenium-server-standalone-3.xx.x.jar
    
    # for Google Chrome
    java -jar -Dwebdriver.chrome.driver=~/chromedriver ~/selenium-server-standalone-3.xx.x.jar
    ``` 
    
    As an alternative way you can use already configured Docker container with older versions of Selenium and Firefox:
    
    ```
    docker run --net=host selenium/standalone-firefox:2.53.0
    ```

5. (Optional) Create `yii2basic_test` database and update it by applying migrations if you have them.

   ```
   tests/bin/yii migrate
   ```

   The database configuration can be found at `config/test_db.php`.


6. Start web server:

    ```
    tests/bin/yii serve
    ```

7. Now you can run all available tests

   ```
   # run all available tests
   vendor/bin/codecept run

   # run acceptance tests
   vendor/bin/codecept run acceptance

   # run only unit and functional tests
   vendor/bin/codecept run unit,functional
   ```

### Code coverage support

By default, code coverage is disabled in `codeception.yml` configuration file, you should uncomment needed rows to be able
to collect code coverage. You can run your tests and collect coverage with the following command:

```
#collect coverage for all tests
vendor/bin/codecept run --coverage --coverage-html --coverage-xml

#collect coverage only for unit tests
vendor/bin/codecept run unit --coverage --coverage-html --coverage-xml

#collect coverage for unit and functional tests
vendor/bin/codecept run functional,unit --coverage --coverage-html --coverage-xml
```

You can see code coverage output under the `tests/_output` directory.
