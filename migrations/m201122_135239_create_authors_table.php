<?php

/*
 * 22.11.2020
 * File: m201122_135239_create_authors_table.php
 * Encoding: UTF-8
 * Project: Test task for "You are not alone"
 * 
 * Author: Gafuroff Alexandr 
 * E-mail: gafuroff.al@yandex.ru
 */

use yii\db\Migration;

/**
 * Handles the creation of table `{{%athors}}`.
 */
class m201122_135239_create_authors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%authors}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%athors}}');
    }
}
