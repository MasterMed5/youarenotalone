<?php

/*
 * 22.11.2020
 * File: m201122_143459_filling_tables_with_data.php
 * Encoding: UTF-8
 * Project: Test task for "You are not alone"
 * 
 * Author: Gafuroff Alexandr 
 * E-mail: gafuroff.al@yandex.ru
 */

use yii\db\Migration;

/**
 * Class m201122_143459_filling_tables_with_data
 */
class m201122_143459_filling_tables_with_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $genries = [
            'Детективная проза',
            'Мемуары',
            'Сказки',
            'Исторические романы',
            'Научная литература'
        ];
        
        foreach($genries as $genre) {
            $this->insert('{{%genries}}', ['name' => $genre]);
        }
        
        $authors = [
            'Иванов Иван Иванович',
            'Петров Петр Петрович',
            'Сидоров Сидор Сидорович',
            'Иванов Петр Сидорович',
            'Петров Сидор Иванович'
        ];
        
        foreach($authors as $author) {
            $this->insert('{{%authors}}', ['name' => $author]);
        }
        
        for($i = 0; $i < 10000; $i++) {
            $this->insert('{{%books}}', [
                'title' => $this->random_lipsum(rand(4, 15), 'words'),
                'date' => date("Y-m-d", mt_rand(0,1606058090)),
                'author' => rand(1, 5),
                'genre' => rand(1, 5),
                'description' => $this->random_lipsum(1, 'paras'),
                'image' => rand(1, 20).'.jpg'               
            ]);
        }
        
    }
    
    private function random_lipsum($amount = 1, $what = 'paras', $start = 0) {
        return simplexml_load_file("http://www.lipsum.com/feed/xml?amount=$amount&what=$what&start=$start")->lipsum;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201122_143459_filling_tables_with_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201122_143459_filling_tables_with_data cannot be reverted.\n";

        return false;
    }
    */
}
