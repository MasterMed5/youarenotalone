<?php

/*
 * 22.11.2020
 * File: m201122_141208_create_books_table.php
 * Encoding: UTF-8
 * Project: Test task for "You are not alone"
 * 
 * Author: Gafuroff Alexandr 
 * E-mail: gafuroff.al@yandex.ru
 */

use yii\db\Migration;

/**
 * Handles the creation of table `{{%books}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%authors}}`
 * - `{{%genries}}`
 */
class m201122_141208_create_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%books}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'date' => 'DATE',
            'author' => $this->integer(),
            'genre' => $this->integer(),
            'description' => $this->text(),
            'image' => $this->string(255)
        ]);

        // creates index for column `author`
        $this->createIndex(
            '{{%idx-books-author}}', '{{%books}}', 'author'
        );

        // add foreign key for table `{{%authors}}`
        $this->addForeignKey(
            '{{%fk-books-author}}', '{{%books}}', 'author', '{{%authors}}', 'id', 'CASCADE'
        );

        // creates index for column `genre`
        $this->createIndex(
            '{{%idx-books-genre}}', '{{%books}}', 'genre'
        );

        // add foreign key for table `{{%genries}}`
        $this->addForeignKey(
            '{{%fk-books-genre}}', '{{%books}}', 'genre', '{{%genries}}', 'id', 'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%authors}}`
        $this->dropForeignKey(
            '{{%fk-books-author}}', '{{%books}}'
        );

        // drops index for column `author`
        $this->dropIndex(
            '{{%idx-books-author}}', '{{%books}}'
        );

        // drops foreign key for table `{{%genries}}`
        $this->dropForeignKey(
            '{{%fk-books-genre}}', '{{%books}}'
        );

        // drops index for column `genre`
        $this->dropIndex(
            '{{%idx-books-genre}}', '{{%books}}'
        );

        $this->dropTable('{{%books}}');
    }
}
